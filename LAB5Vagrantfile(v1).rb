# -*- mode: ruby -*-
# vi: set ft=ruby :

# Configuration de Vagrant pour 3 VMs

Vagrant.configure("2") do |config|

  # Définition des variables communes
  vbox = "ubuntu/xenial64"
  v_ram = "1024"
  v_cpu = 1
  starting_ip = 10
  v_name = ["lb", "web1", "web2"]

  # Configuration de chaque VM à partir des variables à l'aide d'une boucle
  (0..2).each do |i|
    config.vm.define "vm#{i + 1}" do |node|
      node.vm.box = vbox
      node.vm.hostname = v_name[i]
      node.vm.network "private_network", ip: "10.0.0.#{starting_ip + i}"
      
      node.vm.provider "virtualbox" do |vb|
        vb.memory = v_ram
        vb.cpus = v_cpu
      end
    end
  end
  
# Cette boucle crée trois machines virtuelles avec des configurations spécifiques.
# La boucle itère sur une plage de nombres de 0 à 2 inclusivement, créant ainsi trois machines virtuelles.
# La variable |i| prend successivement les valeurs 0, 1 et 2 lors des itérations.
# Pour chaque itération, une machine virtuelle est définie avec un nom unique ("vm1", "vm2", "vm3") en utilisant la valeur de i.
# Configuration de la machine virtuelle dans le contexte de la boucle :
# - La boîte Vagrant à utiliser est définie.
# - Un nom d'hôte est attribué à la machine virtuelle en utilisant le nom correspondant dans la liste hostnames.
# - Une interface réseau privée est configurée avec une adresse IP calculée en ajoutant la valeur de starting_ip à i.
# - La configuration spécifique au fournisseur VirtualBox est définie, notamment la quantité de RAM et le nombre de cœurs de processeur alloués.

  # Configuration de la connexion SSH sur chaque VM
  config.ssh.insert_key = true

# Pour vous connecter en SSH à une VM après 'vagrant up', utilisez:
# vagrant ssh <nom_de_la_VM>
# Remplacez <nom_de_la_VM> par le nom de la machine virtuelle spécifique.

  # Provision
  # config.vm.provision "shell", path: "provision.sh"
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt update
    sudo apt install nginx -y
    sudo service nginx start
    sudo systemctl enable nginx 
  SHELL
end
