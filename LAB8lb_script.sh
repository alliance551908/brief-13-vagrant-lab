#!/bin/bash

# Affiche un message pour indiquer le début de la provision de la machine lb1
echo 'Starting Provision: lb1'

# Met à jour les paquets disponibles pour l'installation
sudo apt-get update

# Installe le serveur web Nginx
sudo apt-get install -y nginx

# Arrête le service Nginx
sudo service nginx stop

# Supprime le fichier de configuration par défaut de Nginx
sudo rm -rf /etc/nginx/sites-enabled/default

# Crée un nouveau fichier de configuration pour Nginx
sudo touch /etc/nginx/sites-enabled/default

# Ajoute la configuration du proxy et des serveurs back-end dans le fichier de configuration de Nginx
echo "upstream testapp {
        server 10.0.0.11;  # Adresse IP du serveur web1
        server 10.0.0.12;  # Adresse IP du serveur web2
}

server {
        listen 80 default_server;
        listen [::]:80 default_server ipv6only=on;

        root /usr/share/nginx/html;
        index index.html index.htm;

        # Make site accessible from http://localhost/
        server_name localhost;

        location / {
                proxy_pass http://testapp;  # Redirige les requêtes vers les serveurs back-end définis dans upstream
        }

}" >> /etc/nginx/sites-enabled/default  # Ajoute la configuration au fichier de configuration de Nginx

# Redémarre le service Nginx pour appliquer les changements de configuration
sudo service nginx start

# Crée un fichier HTML de test pour indiquer le bon fonctionnement de la machine lb1
echo "Machine: lb1" > /var/www/html/index.html

# Affiche un message pour indiquer que la provision de la machine lb1 est terminée
echo 'Provision lb1 complete'

# Rappel : Sur Windows, utilisez 'attrib +x' pour ajouter les permissions d'exécution
