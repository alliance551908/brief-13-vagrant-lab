# -*- mode: ruby -*-
# vi: set ft=ruby :

# Configuration de Vagrant pour 1 VM

Vagrant.configure("2") do |config|
  
  # Configuration de la VM
  config.vm.box = "ubuntu/xenial64"
  config.vm.hostname = "vm1"
  
  config.vm.network "private_network", ip: "10.0.0.10"
  
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "1024"
    vb.cpus = 1
  end
  
  # Exécution du playbook Ansible local
  config.vm.provision "ansible_local" do |ansible|
    ansible.playbook = "nginx.yaml"
  end
  
end