# Brief 13
## Vagrant Lab 1



### Explication de certaines commandes Vagrant :

1. `vagrant init` :
   - Cette commande initialise un nouveau Vagrantfile dans le répertoire actuel. Un Vagrantfile est un fichier de configuration qui décrit les paramètres de la machine virtuelle Vagrant. Cela crée simplement un fichier de configuration par défaut.

2. `vagrant validate` :
   - Cette commande vérifie la validité du Vagrantfile en regardant sa syntaxe et sa structure. Elle peut être utilisée pour détecter les erreurs dans le fichier de configuration.

3. `vagrant up` :
   - Cette commande démarre la machine virtuelle Vagrant en fonction des configurations spécifiées dans le Vagrantfile. Si la machine virtuelle n'est pas encore créée, cette commande la crée et la démarre.

4. `vagrant status` :
   - Cette commande affiche l'état actuel de la machine virtuelle Vagrant dans le répertoire de travail. Elle indique si la machine virtuelle est en cours d'exécution, arrêtée ou suspendue.

5. `vagrant global-status` :
   - Cette commande affiche l'état de toutes les machines virtuelles Vagrant qui sont gérées sur votre système, pas seulement celles dans le répertoire de travail actuel.

6. `vagrant ssh` :
   - Cette commande nous connecte à la machine virtuelle Vagrant via SSH. Cela suppose que la machine virtuelle est en cours d'exécution.

7. `vagrant halt` :
   - Cette commande arrête proprement la machine virtuelle Vagrant en cours d'exécution. Elle sauvegarde l'état actuel de la machine virtuelle et la ferme.

8. `vagrant destroy` :
   - Cette commande supprime complètement la machine virtuelle Vagrant. Toutes les traces de la machine virtuelle sont supprimées, y compris les fichiers de configuration et les disques virtuels.

9. `vagrant box add ubuntu/trusty64` :
   - Cette commande télécharge une box Vagrant à partir de la bibliothèque de boxes Vagrant et l'ajoute localement. Une box est une image préconfigurée d'une machine virtuelle. Dans cet exemple, la box téléchargée est basée sur Ubuntu Trusty Tahr (14.04).

10. `vagrant init ubuntu/trusty64 -h` :
   - Cette commande initialise un nouveau Vagrantfile basé sur la box spécifiée (`ubuntu/trusty64`). L'option `-h` est utilisée pour afficher de l'aide sur la manière d'utiliser la commande `vagrant init` avec cette box spécifique.

Lorsque l'on exécute la commande `vagrant box add ubuntu/trusty64`, Vagrant télécharge la box Ubuntu Trusty Tahr (14.04) depuis la bibliothèque de boxes Vagrant et l'ajoute localement sur votre ordinateur. Les boxes téléchargées sont généralement stockées dans le répertoire `.vagrant.d` dans votre répertoire d'utilisateur (~\.vagrant.d\boxes\).

## Vagrant Lab 2



### Création et Configuration d'une Machine Virtuelle avec Vagrant

1. **Créer un Répertoire :**
   - Ouvrez un terminal et exécutez la commande `mkdir LAB2` pour créer un nouveau répertoire appelé LAB2.

2. **Accéder au Répertoire :**
   - Utilisez la commande `cd LAB2` pour accéder au répertoire que vous venez de créer.

3. **Initialiser Vagrant :**
   - Exécutez `vagrant init` dans le répertoire LAB2 pour initialiser un nouveau fichier de configuration Vagrant.

4. **Configurer le Fichier Vagrant :**
   - Modifiez le fichier Vagrantfile pour décommenter et ajuster les paramètres nécessaires, comme spécifier la version de la box à utiliser:
    
   ```ruby
   Vagrant.configure("2") do |config|
     config.vm.box = "ubuntu/trusty64"
     config.vm.box_version = "20190425.0.0"
   end

5. **Démarrer la Machine Virtuelle :**
   - Exécutez `vagrant up` pour démarrer la machine virtuelle. Cela téléchargera et installera toutes les dépendances nécessaires pour exécuter la VM avec succès.

6. **Installation de Nginx :**
   - Connectez-vous à la machine virtuelle en exécutant `vagrant ssh`.
   - Mettez à jour les paquets Ubuntu en exécutant `sudo apt update`.
   - Installez Nginx en exécutant `sudo apt install nginx`.
   - Démarrez le serveur Nginx en exécutant `sudo service nginx start`.
   - Vérifiez l'état du serveur Nginx en exécutant `sudo service nginx status`.

7. **Exportation vers Vagrant Cloud :**
   - Arrêtez la machine virtuelle avec `vagrant halt`.
   - Exportez la machine virtuelle avec `vagrant package --output nginx.box`.
   - Créez un compte sur Vagrant Cloud si vous n'en avez pas déjà un.
   - Créez un nouveau fournisseur sur Vagrant Cloud.
   - Téléversez le package avec `vagrant cloud publish --release NOM_UTILISATEUR/NOM_BOX VERSION PROVIDER CHEMIN_FICHIER.box`. 
     Assurez-vous de remplacer NOM_UTILISATEUR, NOM_BOX, VERSION, PROVIDER et CHEMIN_FICHIER.box par les valeurs appropriées.
   (https://app.vagrantup.com/plaquistes_cedres_0i/boxes/nginx1/versions/1/providers/virtualbox/amd64/vagrant.box)

## Vagrant Lab 3



### Guide d'utilisation de Vagrant pour GitLab

| Étapes                                             | Description                                                                                                  |
|----------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| **1. Créer un Répertoire :**                       | Ouvrez un terminal et exécutez la commande `mkdir LAB3` pour créer un nouveau répertoire appelé LAB3.      |
| **2. Accéder au Répertoire :**                     | Utilisez la commande `cd LAB3` pour accéder au répertoire que vous venez de créer.                           |
| **3. Créer un Vagrantfile minimaliste :**         | Vous pouvez suivre ces étapes simples :                                                                      |
|                                                    | ```powershell                                                                                                |
|                                                    | Set-Content -Path "Vagrantfile" -Value 'Vagrant.configure("2") do \| config|'                                |
|                                                    | Add-Content -Path "Vagrantfile" -Value '  config.vm.box = "geerlingguy/centos7"'                            |
|                                                    | Add-Content -Path "Vagrantfile" -Value '  config.vm.provider "virtualbox" do \|vb\|'                         |
|                                                    | Add-Content -Path "Vagrantfile" -Value '    vb.memory = "2048"'                                             |
|                                                    | Add-Content -Path "Vagrantfile" -Value '    vb.cpus = 2'                                                    |
|                                                    | Add-Content -Path "Vagrantfile" -Value '  end'                                                              |
|                                                    | Add-Content -Path "Vagrantfile" -Value 'end'                                                                |
|                                                    |                                                                                                              |
|                                                    | Maintenant, le fichier Vagrant devrait ressembler à ça :                                                     |
|                                                    |                                                                                                              |
|                                                    | ```ruby                                                                                                      |
|                                                    | Vagrant.configure("2") do \| config|                                                                           |
|                                                    |   config.vm.box = "geerlingguy/centos7"                                                                     |
|                                                    |   config.vm.provider "virtualbox" do \|vb\|                                                                  |
|                                                    |     vb.memory = "2048"                                                                                      |
|                                                    |     vb.cpus = 2                                                                                             |
|                                                    |   end                                                                                                        |
|                                                    | end                                                                                                          |
| **4. Variabiliser les paramètres indiqués ci-dessus :** | Vous pouvez définir des variables pour les paramètres utilisés dans le Vagrantfile :                        |
|                                                    |                                                                                                              |
|                                                    | ```ruby                                                                                                      |
|                                                    | # Définition des variables                                                                                  |
|                                                    | centos_box = "geerlingguy/centos7"                                                                          |
|                                                    | cpu_count = 2                                                                                               |
|                                                    | ram_size = "2048"                                                                                           |
|                                                    |                                                                                                              |
|                                                    | # Configuration de Vagrant                                                                                   |
|                                                    | Vagrant.configure("2") do \| config|                                                                           |
|                                                    |   config.vm.box = centos_box                                                                                |
|                                                    |                                                                                                              |
|                                                    |   config.vm.provider "virtualbox" do \|vb\|                                                                  |
|                                                    |     vb.memory = ram_size                                                                                     |
|                                                    |     vb.cpus = cpu_count                                                                                      |
|                                                    |   end                                                                                                        |
|                                                    | end                                                                                                          |
| **5. Démarrer la Machine Virtuelle :**            | Exécutez `vagrant up` pour démarrer la machine virtuelle. Cela téléchargera et installera toutes les dépendances nécessaires pour exécuter la VM avec succès. |
| **6. Installation de Nginx :**                    | - Connectez-vous à la machine virtuelle en exécutant `vagrant ssh`.                                           |
|                                                    | - Mettez à jour les paquets Ubuntu en exécutant `sudo apt update`.                                           |
|                                                    | - Installez Nginx en exécutant `sudo apt install nginx`.                                                      |
|                                                    | - Démarrez le serveur Nginx en exécutant `sudo service nginx start`.                                          |
|                                                    | - Vérifiez l'état du serveur Nginx en exécutant `sudo service nginx status`.                                  |

## Vagrant Lab 4



### Guide d'utilisation de Vagrant pour GitLab

| Étapes                                             | Description                                                                                                  |
|----------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| **1. Créer un Répertoire :**                       | Ouvrez un terminal et exécutez la commande `mkdir LAB4` pour créer un nouveau répertoire appelé LAB4.      |
| **2. Accéder au Répertoire :**                     | Utilisez la commande `cd LAB4` pour accéder au répertoire que vous venez de créer.                           |
| **3. Créer un Vagrantfile :**                      | Ouvrez un éditeur de texte et créez un nouveau fichier nommé `Vagrantfile`.                                 |
|                                                    | Ajoutez-y le code suivant :                                                                                  |
| ```ruby                                           |                                                                                                              |
| # Définition des variables                        |                                                                                                              |
| centos_box = "geerlingguy/centos7"                |                                                                                                              |
| cpu_count = 2                                      |                                                                                                              |
| ram_size = "2048"                                  |                                                                                                              |
| private_ip = "10.0.0.10"                           |                                                                                                              |
|                                                                                                              |                                                                                                              |
| # Configuration de Vagrant                         |                                                                                                              |
| Vagrant.configure("2") do \| config|                |                                                                                                              |
|   config.vm.box = centos_box                      |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   config.vm.provider "virtualbox" do \|vb\|        |                                                                                                              |
|     vb.memory = ram_size                           |                                                                                                              |
|     vb.cpus = cpu_count                            |                                                                                                              |
|   end                                              |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   config.vm.network "private_network", ip: private_ip |                                                                                                          |
|                                                                                                              |                                                                                                              |
|   # Configuration de la connexion SSH             |                                                                                                              |
|   config.ssh.insert_key = false                   |                                                                                                              |
| end                                                |                                                                                                              |
| ```                                                |                                                                                                              |
|                                                                                                              |                                                                                                              |
| Enregistrez le fichier `Vagrantfile`. Assurez-vous que votre fichier `Vagrantfile` a l'extension correcte. Un fichier `Vagrantfile` doit être enregistré avec l'extension `.rb`. Par exemple, `Vagrantfile.rb`. |                                                                                  |
| Ouvrez le fichier `Vagrantfile` dans Notepad++.   |                                                                                                              |
| Cliquez sur "Fichier" dans la barre de menu.      |                                                                                                              |
| Sélectionnez "Enregistrer sous..." dans le menu déroulant. |                                                                                                          |
| Dans la boîte de dialogue qui s'ouvre, assurez-vous que le champ "Nom du fichier" contient `Vagrantfile.rb`.|                                                           |
| Cliquez sur le bouton "Enregistrer" pour enregistrer le fichier avec l'extension `.rb`.                     |                                                                                                              |
| **4. Démarrer la Machine Virtuelle :**            | Exécutez `vagrant up` pour démarrer la machine virtuelle. Cela téléchargera et installera toutes les dépendances nécessaires pour exécuter la VM avec succès. |
| **5. Installation de Nginx :**                    | - Connectez-vous à la machine virtuelle en exécutant `vagrant ssh`.                                           |
|                                                    | - Mettez à jour les paquets Ubuntu en exécutant `sudo yum update`.                                           |
|                                                    | - Installez Nginx en exécutant `sudo yum install nginx`.                                                      |
|                                                    | - Démarrez le serveur Nginx en exécutant `sudo systemctl start nginx`.                                                      |
|                                                    | - Activez le démarrage automatique de Nginx en exécutant `sudo systemctl enable nginx`.                                          |
|                                                    | - Vérifiez l'état du serveur Nginx en exécutant `sudo systemctl status nginx`.                                  |

## Vagrant Lab 5



### Guide d'utilisation de Vagrant pour GitLab

| Étapes                                             | Description                                                                                                  |
|----------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| **1. Créer un Répertoire :**                       | Ouvrez un terminal et exécutez la commande `mkdir LAB5` pour créer un nouveau répertoire appelé LAB5.      |
| **2. Accéder au Répertoire :**                     | Utilisez la commande `cd LAB5` pour accéder au répertoire que vous venez de créer.                           |
| **3. Créer un Vagrantfile :**                     | Ouvrez un éditeur de texte et créez un nouveau fichier nommé `Vagrantfile`.                                 |
|                                                    | Ajoutez-y le code suivant pour configurer 3 VMs :                                                            |
| ```ruby                                           |                                                                                                              |
| # Configuration de Vagrant pour 3 VMs             |                                                                                                              |
| Vagrant.configure("2") do \| config|                |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   # Définition des variables communes            |                                                                                                              |
|   box_name = "ubuntu/xenial64"                   |                                                                                                              |
|   ram_size = "1024"                               |                                                                                                              |
|   cpu_count = 1                                   |                                                                                                              |
|   starting_ip = 10                                |                                                                                                              |
|   hostnames = ["lb", "web1", "web2"]              |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   # Configuration de chaque VM à partir des variables                                                       |                                                                                                              |
|   (0..2).each do \|i\|                             |                                                                                                              |
|     config.vm.define "vm#{i + 1}" do \| node|      |                                                                                                              |
|       node.vm.box = box_name                     |                                                                                                              |
|       node.vm.hostname = hostnames[i]            |                                                                                                              |
|       node.vm.network "private_network", ip: "10.0.0.#{starting_ip + i}"                                    |                                                                                                              |
|       node.vm.provider "virtualbox" do \|vb\|     |                                                                                                              |
|         vb.memory = ram_size                     |                                                                                                              |
|         vb.cpus = cpu_count                      |                                                                                                              |
|       end                                        |                                                                                                              |
|     end                                          |                                                                                                              |
|   end                                            |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   # Configuration de la connexion SSH sur chaque VM                                                           |                                                                                                              |
|   config.ssh.insert_key = true                   |                                                                                                              |
| end                                              |                                                                                                              |
| ```                                              |                                                                                                              |
|                                                                                                              |                                                                                                              |
| Enregistrez le fichier `Vagrantfile`.            |                                                                                                              |
| Exécutez `vagrant up` pour démarrer les 3 VMs.  |                                                                                                              
OR                                                                                                              
|```ruby                                           |                                                                                                              |
| # Méthode pour configurer une machine virtuelle   |                                                                                                              |
| def configurer_vm(config, nom, ip)                |                                                                                                              |
|   config.vm.define nom do \|vm\|                   |                                                                                                              |
|     vm.vm.box = "ubuntu/xenial64"                |                                                                                                              |
|     vm.vm.hostname = nom                         |                                                                                                              |
|     vm.vm.network "private_network", ip: ip      |                                                                                                              |
|     vm.vm.provider "virtualbox" do \|vb\|         |                                                                                                              |
|       vb.memory = "1024"                         |                                                                                                              |
|       vb.cpus = 1                                |                                                                                                              |
|     end                                          |                                                                                                              |
|   end                                            |                                                                                                              |
| end                                              |                                                                                                              |
|                                                                                                              |                                                                                                              |
| # Configuration de Vagrant pour 3 VMs             |                                                                                                              |
| Vagrant.configure("2") do \| config|                |                                                                                                              |
|   # Configuration de chaque VM en appelant la méthode configurer_vm avec les arguments appropriés          |                                                                                                              |
|   configurer_vm(config, "lb", "10.0.0.10")       |                                                                                                              |
|   configurer_vm(config, "web1", "10.0.0.11")     |                                                                                                              |
|   configurer_vm(config, "web2", "10.0.0.12")     |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   # Configuration de la connexion SSH sur chaque VM                                                           |                                                                                                              |
|   config.ssh.insert_key = true                   |                                                                                                              |
| end                                              |                                                                                                              |
| ```                                              |                                                                                                              |
|                                                                                                              |                                                                                                              |
| Enregistrez le fichier `Vagrantfile`.            |                                                                                                              |
| Exécutez `vagrant up` pour démarrer les 3 VMs.  |                                                                                                              |
| **4. Démarrer la Machine Virtuelle :**           | Exécutez `vagrant up` pour démarrer la machine virtuelle. Cela téléchargera et installera toutes les dépendances nécessaires pour exécuter la VM avec succès. |
| **5. Installation de Nginx :**                   | - Connectez-vous à la machine virtuelle en exécutant `vagrant ssh`.                                           |
|                                                    | - Mettez à jour les paquets Ubuntu en exécutant `sudo apt update`.                                           |
|                                                    | - Installez Nginx en exécutant `sudo apt install nginx`.                                                      |
|                                                    | - Démarrez le serveur Nginx en exécutant `sudo service nginx start`.                                          |
|                                                    | - Vérifiez l'état du serveur Nginx en exécutant `sudo service nginx status`.                                  |

# Vagrant Lab 7



## Guide d'utilisation de Vagrant pour GitLab

| Étapes                                             | Description                                                                                                  |
|----------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| **1. Créer un Répertoire :**                       | Ouvrez un terminal et exécutez la commande `mkdir LAB7` pour créer un nouveau répertoire appelé LAB7.      |
| **2. Accéder au Répertoire :**                     | Utilisez la commande `cd LAB7` pour accéder au répertoire que vous venez de créer.                           |
| **3. Créer un Vagrantfile :**                     | Ouvrez un éditeur de texte et créez un nouveau fichier nommé `Vagrantfile`.                                 |
|                                                    | Ajoutez-y le code suivant pour configurer une VM :                                                           |
| | ```ruby                                              |                                                                                                              |
| # Configuration de Vagrant pour une VM               |                                                                                                              |
| Vagrant.configure("2") do \| config|                   |                                                                                                              |
|   config.vm.box = "<username>/nginx"                 | Remplacez `<username>` par votre nom d'utilisateur.                                                          |
|   config.vm.hostname = "LAB7"                       |                                                                                                              |
|   config.vm.network "private_network", ip: "10.0.0.10" |                                                                                                            |
|                                                       |                                                                                                              |
|   config.vm.provider "virtualbox" do \|vb\|           |                                                                                                              |
|     vb.memory = "1024"                               |                                                                                                              |
|     vb.cpus = 1                                      |                                                                                                              |
|   end                                               |                                                                                                              |
|                                                       |                                                                                                              |
|   # Configuration de la connexion SSH sur la VM      |                                                                                                              |
|   config.ssh.insert_key = true                       |                                                                                                              |
|                                                       |                                                                                                              |
|   # Partage de dossier pour monter le code de l'application |                                                                                                          |
|   config.vm.synced_folder "/chemin/vers/votre/dossier/LAB7", "/var/www/html" |                               |
|                                                       |                                                                                                              |
|   # Provisionneur pour créer le répertoire /var/www/html s'il n'existe pas |                                  |
|   config.vm.provision "shell", inline: "sudo mkdir -p /var/www/html" |                                       |
| end                                               |                                                                                                              |
| ```                                                   |                                                                                                              |
|                                                                                                              |                                                                                                              |
| Enregistrez le fichier `Vagrantfile`.            |                                                                                                              |
| **4. Récupérer l'application :**                  | Assurez-vous d'avoir l'application `static-website-example` localement dans votre dossier `LAB7`.           |
| **5. Montez le code de l'application :**          | Utilisez l'une des méthodes suivantes pour monter le code de l'application dans la VM :                     |
|                                                    | - 1. Utilisez la fonctionnalité de partage de dossiers de Vagrant.                                              |
|                                                    | - 2. Utilisez les commandes provisioners de Vagrant.                                                            |
| **6. Vérifier l'accessibilité du site :**         | Ouvrez un navigateur web et accédez à l'adresse `http://10.0.0.10` pour vérifier que le site est accessible.  |

| Méthodes de Configuration Nginx |
|---------------------------------|

| Étapes                                               | Commandes                                                                                         |
|------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| **SOLUTION 1 : Modification du fichier default**                  |                                                                                                  |
| 1. Ouvrez le fichier `default` avec un éditeur de texte en mode superutilisateur | `sudo nano /etc/nginx/sites-available/default`                                                |
|                                                     | Modifiez le fichier pour définir le chemin racine et les règles de routage nécessaires. Voici un exemple de configuration modifiée : |
|                                                     | ```nginx                                                                                         |
|                                                     | server {                                                                                         |
|                                                     |     listen 80 default_server;                                                                    |
|                                                     |     listen [::]:80 default_server ipv6only=on;                                                  |
|                                                     |                                                                                                  |
|                                                     |     root /var/www/html;                                                                          |
|                                                     |     index index.html;                                                                            |
|                                                     |                                                                                                  |
|                                                     |     # Make site accessible from http://localhost/                                               |
|                                                     |     server_name localhost;                                                                       |
|                                                     |                                                                                                  |
|                                                     |     location / {                                                                                 |
|                                                     |         try_files $uri $uri/ =404;                                                               |
|                                                     |     }                                                                                            |
|                                                     | }                                                                                                |
|                                                     |                                                                                                  |
|                                                     | Enregistrez les modifications et quittez l'éditeur de texte (Ctrl + X, Y, Entrée).              |
|                                                     |                                                                                                  |
|                                                     | Redémarrez Nginx :                                                                              |
|                                                     | `sudo service nginx restart`                                                                    |
| **SOLUTION 2 : Création d'un nouveau fichier de configuration**   |                                                                                                  |
| - Créez et configurez un fichier LAB7 dans `/etc/nginx/sites-available/` avec le contenu suivant :        |                                                                                                  |
| ```nginx                                            | server {                                                                                         |
|                                                     |     listen 80;                                                                                   |
|                                                     |     server_name 10.0.0.10;  # Remplacez ceci par l'adresse IP de votre machine                   |
|                                                     |                                                                                                  |
|                                                     |     root /var/www/html;                                                                          |
|                                                     |     index index.html;                                                                            |
|                                                     |                                                                                                  |
|                                                     |     location / {                                                                                 |
|                                                     |         try_files $uri $uri/ =404;                                                               |
|                                                     |     }                                                                                            |
|                                                     | }                                                                                                |
|                                                     | ```                                                                                              |
|                                                     |                                                                                                  |
|                                                     | Assurez-vous de créer un lien symbolique vers `/etc/nginx/sites-enabled/` :                      |
|                                                     | `sudo ln -s /etc/nginx/sites-available/LAB7 /etc/nginx/sites-enabled/`                           |
|                                                     |                                                                                                  |
|                                                     | Redémarrez Nginx :                                                                              |
|                                                     | `sudo service nginx restart`                                                                    |
# Vagrant Lab 8



## Guide d'utilisation de Vagrant pour GitLab

| Étapes                                             | Description                                                                                                  |
|----------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| **1. Créer un Répertoire :**                       | Ouvrez un terminal et exécutez la commande `mkdir LAB8` pour créer un nouveau répertoire appelé LAB8.      |
| **2. Accéder au Répertoire :**                     | Utilisez la commande `cd LAB8` pour accéder au répertoire que vous venez de créer.                           |
| **3. Créer un Vagrantfile :**                     | Ouvrez un éditeur de texte et créez un nouveau fichier nommé `Vagrantfile`.                                 |
|                                                    | Ajoutez-y le code suivant pour configurer 3 VMs et exécuter les scripts donnés :                            |
| ```ruby                                           |                                                                                                              |
| # Configuration de Vagrant pour 3 VMs             |                                                                                                              |
| Vagrant.configure("2") do \| config|                |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   # Configuration de la première VM (lb)         |                                                                                                              |
|   config.vm.define "lb" do \|lb\|                  |                                                                                                              |
|     lb.vm.box = "ubuntu/xenial64"                |                                                                                                              |
|     lb.vm.hostname = "lb"                        |                                                                                                              |
|     lb.vm.network "private_network", ip: "10.0.0.10" |                                                                                                            |
|     lb.vm.provider "virtualbox" do \|vb\|         |                                                                                                              |
|       vb.memory = "1024"                         |                                                                                                              |
|       vb.cpus = 1                                |                                                                                                              |
|     end                                          |                                                                                                              |
|     lb.vm.provision "shell", path: "lb_script.sh" |                                                                                                              |
|   end                                            |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   # Configuration de la deuxième VM (web1)       |                                                                                                              |
|   config.vm.define "web1" do \|web1\|              |                                                                                                              |
|     web1.vm.box = "ubuntu/xenial64"              |                                                                                                              |
|     web1.vm.hostname = "web1"                    |                                                                                                              |
|     web1.vm.network "private_network", ip: "10.0.0.11" |                                                                                                            |
|     web1.vm.provider "virtualbox" do \|vb\|       |                                                                                                              |
|       vb.memory = "1024"                         |                                                                                                              |
|       vb.cpus = 1                                |                                                                                                              |
|     end                                          |                                                                                                              |
|     web1.vm.provision "shell", path: "web_script.sh", args: ["1"] |                                                                                                      |
|   end                                            |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   # Configuration de la troisième VM (web2)      |                                                                                                              |
|   config.vm.define "web2" do \|web2\|              |                                                                                                              |
|     web2.vm.box = "ubuntu/xenial64"              |                                                                                                              |
|     web2.vm.hostname = "web2"                    |                                                                                                              |
|     web2.vm.network "private_network", ip: "10.0.0.12" |                                                                                                            |
|     web2.vm.provider "virtualbox" do \|vb\|       |                                                                                                              |
|       vb.memory = "1024"                         |                                                                                                              |
|       vb.cpus = 1                                |                                                                                                              |
|     end                                          |                                                                                                              |
|     web2.vm.provision "shell", path: "web_script.sh", args: ["2"] |                                                                                                      |
|   end                                            |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   # Configuration de la connexion SSH sur chaque VM |                                                                                                              |
|   config.ssh.insert_key = true                   |                                                                                                              |
| end                                              |                                                                                                              |
| ```                                              |                                                                                                              |
|                                                                                                              |                                                                                                              |
| Enregistrez le fichier `Vagrantfile`.            |                                                                                                              |
| Exécutez `vagrant up` pour démarrer les 3 VMs.  |                                                                                                              |
| **4. Accéder à l'interface web de chaque serveur :** |                                                                                                              |
| - Ouvrez un navigateur web et entrez l'adresse IP de chaque serveur dans la barre d'adresse.               |
| - Assurez-vous que les services Web sont en cours d'exécution sur les serveurs Web (VM2 et VM3).           |
| - Confirmez le bon fonctionnement en vérifiant que vous pouvez accéder à l'interface web de chaque serveur. |

# Vagrant Lab 9



## Guide d'utilisation de Vagrant pour GitLab

| Étapes                                             | Description                                                                                                  |
|----------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| **1. Créer un Répertoire :**                       | Ouvrez un terminal et exécutez la commande `mkdir LAB9` pour créer un nouveau répertoire appelé LAB9.      |
| **2. Accéder au Répertoire :**                     | Utilisez la commande `cd LAB9` pour accéder au répertoire que vous venez de créer.                           |
| **3. Créer un Vagrantfile :**                     | Ouvrez un éditeur de texte et créez un nouveau fichier nommé `Vagrantfile`.                                 |
|                                                    | Ajoutez-y le code suivant pour configurer une VM :                                                            |
| ```ruby                                           |                                                                                                              |
| # Configuration de Vagrant pour 1 VM              |                                                                                                              |
| Vagrant.configure("2") do \| config|                |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   # Configuration de la VM                         |                                                                                                              |
|   config.vm.box = "ubuntu/xenial64"               |                                                                                                              |
|   config.vm.hostname = "vm1"                      |                                                                                                              |
|   config.vm.network "private_network", ip: "10.0.0.10" |                                                                                                            |
|   config.vm.provider "virtualbox" do \|vb\|        |                                                                                                              |
|     vb.memory = "1024"                            |                                                                                                              |
|     vb.cpus = 1                                   |                                                                                                              |
|   end                                             |                                                                                                              |
|                                                                                                              |                                                                                                              |
|   # Exécution du playbook Ansible local           |                                                                                                              |
|   config.vm.provision "ansible_local" do \| ansible| |                                                                                                              |
|     ansible.playbook = "nginx.yaml"               |                                                                                                              |
|   end                                             |                                                                                                              |
| end                                                |                                                                                                              |
| ```                                                |                                                                                                              |
|                                                                                                              |                                                                                                              |
| Enregistrez le fichier `Vagrantfile`.             |                                                                                                              |
| Créez un playbook Ansible nommé `nginx.yaml` dans le même répertoire que votre `Vagrantfile`.               |
| Ajoutez-y les tâches nécessaires pour installer Nginx et le démarrer. Assurez-vous que les directives correspondent à votre système d'exploitation choisi. |                                                                                                            |
| Exécutez `vagrant up` pour démarrer la VM et provisionner avec Ansible.                                       |                                                                                                              |
| Vérifiez que la page par défaut de Nginx est accessible via le navigateur en ouvrant `http://10.0.0.10` dans votre navigateur web. |                                         |
