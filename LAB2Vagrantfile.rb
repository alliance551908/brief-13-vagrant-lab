# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  # Box settings
  config.vm.box = "ubuntu/trusty64"
  config.vm.box_version = "20190425.0.0"

  # Update
  # config.vm.box_check_update = false

  # Network
  # config.vm.network "forwarded_port", guest: 80, host: 8080
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
  # config.vm.network "private_network", ip: "192.168.33.10"
  # config.vm.network "public_network"

  # Sync folder
  # config.vm.synced_folder "../data", "/vagrant_data"
  # config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder "C:/Users/Karim/LAB2", "/usr/share/nginx/html"

  # Provider
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end

  # Provision
  # config.vm.provision "shell", path: "provision.sh"
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt update
    sudo apt install nginx -y
    sudo service nginx start
    sudo systemctl enable nginx 
  SHELL
end
