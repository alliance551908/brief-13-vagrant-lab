# -*- mode: ruby -*-
# vi: set ft=ruby :

# Méthode pour configurer une machine virtuelle
def configurer_vm(config, nom, ip)
  config.vm.define nom do |vm|
    vm.vm.box = "ubuntu/xenial64"
    vm.vm.hostname = nom
    vm.vm.network "private_network", ip: ip
    
    vm.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.cpus = 1
    end
  end
end

# Configuration de Vagrant pour 3 VMs
Vagrant.configure("2") do |config|
  # Configuration de chaque VM en appelant la méthode configurer_vm avec les arguments appropriés
  configurer_vm(config, "lb", "10.0.0.10")
  configurer_vm(config, "web1", "10.0.0.11")
  configurer_vm(config, "web2", "10.0.0.12")

  # Configuration de la connexion SSH sur chaque VM
  config.ssh.insert_key = true

# Pour démarrer les machines virtuelles et se connecter à chacune d'elles :
# - Exécutez 'vagrant up' pour démarrer les machines virtuelles.
# - Utilisez 'vagrant ssh [nom_de_la_machine]' pour vous connecter à chaque machine (lb, web1, web2).
# - Après avoir terminé, exécutez 'vagrant halt' pour éteindre les machines virtuelles.

  # Provision
  # config.vm.provision "shell", path: "provision.sh"
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt update
    sudo apt install nginx -y
    sudo service nginx start
    sudo systemctl enable nginx 
  SHELL
end
