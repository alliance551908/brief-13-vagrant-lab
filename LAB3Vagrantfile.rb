# -*- mode: ruby -*-
# vi: set ft=ruby :

# Définition des variables
v_box = "geerlingguy/centos7"
v_cpu = 2
v_ram = "2048"

# Configuration de Vagrant
Vagrant.configure("2") do |config|
  config.vm.box = v_box
  
  config.vm.provider "virtualbox" do |vb|
    vb.memory = v_ram
    vb.cpus = v_cpu
  end

# Provision
 # config.vm.provision "shell", path: "provision.sh"
 config.vm.provision "shell", inline: <<-SHELL
   sudo apt update
   sudo apt install nginx -y
   sudo service nginx start
   sudo systemctl enable nginx 
 SHELL  
end
