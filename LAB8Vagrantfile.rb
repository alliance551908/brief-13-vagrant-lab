# -*- mode: ruby -*-
# vi: set ft=ruby :

# Définition de la configuration Vagrant
Vagrant.configure("2") do |config|
  
  # Configuration de la première machine virtuelle (lb)
  config.vm.define "lb" do |lb|
    # Utilisation de l'image ubuntu/xenial64
    lb.vm.box = "ubuntu/xenial64"
    # Configuration du nom d'hôte
    lb.vm.hostname = "lb"
    # Configuration du réseau privé avec une adresse IP spécifique
    lb.vm.network "private_network", ip: "10.0.0.10"
    # Configuration des ressources de la machine virtuelle (mémoire et CPU)
    lb.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"  # Mémoire RAM de 1024 Mo
      vb.cpus = 1         # 1 CPU
    end
    # Provisionnement de la machine virtuelle en exécutant le script lb_script.sh
    lb.vm.provision "shell", path: "lb_script.sh"
  end

  # Configuration de la deuxième machine virtuelle (web1)
  config.vm.define "web1" do |web1|
    # Utilisation de l'image ubuntu/xenial64
    web1.vm.box = "ubuntu/xenial64"
    # Configuration du nom d'hôte
    web1.vm.hostname = "web1"
    # Configuration du réseau privé avec une adresse IP spécifique
    web1.vm.network "private_network", ip: "10.0.0.11"
    # Configuration des ressources de la machine virtuelle (mémoire et CPU)
    web1.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"  # Mémoire RAM de 1024 Mo
      vb.cpus = 1         # 1 CPU
    end
    # Provisionnement de la machine virtuelle en exécutant le script web_script.sh avec l'argument "1"
    # L'argument "1" indique à la machine web1 qu'elle est le premier serveur dans l'équilibrage de charge
    web1.vm.provision "shell", path: "web_script.sh", args: ["1"]
  end

  # Configuration de la troisième machine virtuelle (web2)
  config.vm.define "web2" do |web2|
    # Utilisation de l'image ubuntu/xenial64
    web2.vm.box = "ubuntu/xenial64"
    # Configuration du nom d'hôte
    web2.vm.hostname = "web2"
    # Configuration du réseau privé avec une adresse IP spécifique
    web2.vm.network "private_network", ip: "10.0.0.12"
    # Configuration des ressources de la machine virtuelle (mémoire et CPU)
    web2.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"  # Mémoire RAM de 1024 Mo
      vb.cpus = 1         # 1 CPU
    end
    # Provisionnement de la machine virtuelle en exécutant le script web_script.sh avec l'argument "2"
    # L'argument "2" indique à la machine web2 qu'elle est le deuxième serveur dans l'équilibrage de charge
    web2.vm.provision "shell", path: "web_script.sh", args: ["2"]
  end

  # Configuration de la connexion SSH
  config.ssh.insert_key = true
end
