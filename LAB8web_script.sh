#!/bin/bash

# Affiche un message pour indiquer le début de la provision de la machine web$1
echo 'Starting Provision: web'$1

# Met à jour les paquets disponibles pour l'installation
sudo apt-get update

# Installe le serveur web Nginx
sudo apt-get install -y nginx

# Crée un fichier HTML contenant un message indiquant le numéro de la machine (web1 ou web2)
echo "<h1>Machine: web"$1 "</h1>" > /var/www/html/index.html

# Affiche un message pour indiquer que la provision de la machine web$1 est terminée
echo 'Provision web'$1 'complete'

# Rappel : Sur Windows, utilisez 'attrib +x' pour ajouter les permissions d'exécution
