# -*- mode: ruby -*-
# vi: set ft=ruby :

# Configuration de Vagrant pour une VM
Vagrant.configure("2") do |config|
  config.vm.box = "plaquistes_cedres_0i/nginx"  # Remplacez <username> par votre nom d'utilisateur
  config.vm.hostname = "LAB7"
  config.vm.network "private_network", ip: "10.0.0.10"
  
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "1024"
    vb.cpus = 1
  end
  
  # Configuration de la connexion SSH sur la VM
  config.ssh.insert_key = true

  # Partage de dossier pour monter le code de l'application
  config.vm.synced_folder "/chemin/vers/votre/dossier/lab-7", "/var/www/html"

  # Provisionneur pour créer le répertoire /var/www/html s'il n'existe pas
  config.vm.provision "shell", inline: "sudo mkdir -p /var/www/html"
end
